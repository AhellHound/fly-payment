package com.ahellhound.bukkit.flypayment;

import org.bukkit.entity.Player;

public class Flight {
    // Messages constructor
    private Messages Messages = new Messages();
    // Scheduler constructor
    private Scheduler Scheduler = new Scheduler();
    // risemc constructor
    private Timers Timers = new Timers();
    //Configuration
    Configuration config = new Configuration();

    // Enables flight
    public void enableFlight(Player p, int tier) {
        
        p.setAllowFlight(true);
        p.setFlying(true);
        //Test
        //p.setFlySpeed(config.getFlySpeed(tier));
        // adds player to withdraw array
        Timers.addWithdrawArray(p, tier);
        Timers.addFlyCheckArray(p);
        // Adds player to hashmap to find time left on flight
        // Scheduler.addPlayerTimeLeft(p, System.currentTimeMillis());
        Scheduler.addSafePlayer(p);
    }

    // Disables flight
    /**
     * *@param reason 0 = Fly Off; 1 = Free Fly Off; 2 = In PvP Zone; 3 =
     * Banned; 4 = No Resources Left;
     */
    public void disableFlight(Player p, int tier, int reason) {
        // disallows flying
        p.setAllowFlight(false);
        // stops player from flying
        p.setFlying(false);
        // removes player from to array
        Timers.removeWithdrawArray(p, tier);
        Timers.removeFlyCheckArray(p);
        // sends player disable flight message
        p.sendMessage(Messages.flyingDisabledMessage(p, reason));
        // TODO
        // p.sendMessage( Messages.flyingDisabledRanOutMessage(p));

    }

    // disables freeflight
    /**
     * *@param reason 0 = Fly Off; 1 = Free Fly Off; 2 = In PvP Zone; 3 =
     * Banned; 4 = No Resources Left;
     */
    public void disableFreeFlight(Player p) {
        // disallows flying
        p.setAllowFlight(false);
        // stops player from flying
        p.setFlying(false);
        p.sendMessage(Messages.flyingDisabledMessage(p, 1));
        // removes player from free fly array
        Timers.removeFromArrays(p);
        // sends player disable flight message
    }

    // disables freeflight
    public void enableFreeFlight(Player p) {
        // disallows flying
        p.setAllowFlight(true);
        // stops player from flying
        p.setFlying(true);
        // removes player from free fly array
        Timers.addFreeFlyArray(p);
        Timers.addFlyCheckArray(p);
        // sends player disable flight message
        p.sendMessage(Messages.freeFlightEnabled(p));
        Scheduler.addSafePlayer(p);
    }

    public void enableFlightNoCharge(Player p) {
        // allows players to fly
        p.setAllowFlight(true);
        // makes players fly
        p.setFlying(true);
        // Adds to safe player scheduler
        Scheduler.addSafePlayer(p);
        Scheduler.addSafePlayer(p); 

    }

    
    
    public int getDisableReason(Player p) {

        BanList banL = new BanList();

        if (banL.isInBannedWorld(p)) {

            return 7;
            
        } else if (banL.isBannedFly(p.getName())) {
            
            return 3;
        }

        return 0;

    }

}
