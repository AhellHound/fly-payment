package com.ahellhound.bukkit.flypayment;

import org.bukkit.entity.Player;

//import org.bukkit.plugin.java.JavaPlugin;

public class Configuration /* extends JavaPlugin */{

    public boolean getReloadSafetyTeleport() {
        boolean reloadSafetyTeleport;
        reloadSafetyTeleport = Main.getInstance().getConfig().getBoolean("OnReloadTeleportToGround");
        return reloadSafetyTeleport;
    }

    public String getItemChargeEnum(int tier) {
        String itemChargeEnum;
        itemChargeEnum = Main.getInstance().getConfig().getString("ChargeItemNameTier" + tier);
        return itemChargeEnum;
    }

    public int getItemChargeAmount(int tier) {
        int itemChargeAmount;
        itemChargeAmount = Main.getInstance().getConfig().getInt("AmountOfItemsToChargeTier" + tier);
        return itemChargeAmount;
    }

    public long getTimerAmount(int tier) {
        long timerAmount;
        timerAmount = Main.getInstance().getConfig().getLong("FlightTimerTier" + tier);
        return timerAmount;
    }

    public int getBanCheckTimer() {
        int timerAmount;
        timerAmount = Main.getInstance().getConfig().getInt("BanCheckTimer");
        return timerAmount;
    }

    public int getExpChargeAmount(int tier) {
        int expChargeAmount;
        expChargeAmount = Main.getInstance().getConfig().getInt("ChargeEXPTier" + tier);
        return expChargeAmount;
    }

    public int getMoneyChargeAmount(int tier) {
        int moneyChargeAmount;
        moneyChargeAmount = Main.getInstance().getConfig().getInt("ChargeMoneyTier" + tier);
        return moneyChargeAmount;
    }

    public boolean getEconomyAccount(int tier) {
        boolean economyAccount;
        economyAccount = Main.getInstance().getConfig().getBoolean("EconomyAccountTier" + tier);
        return economyAccount;
    }

    public String getEconomyAccountName(int tier) {
        String economyAccountName;
        economyAccountName = Main.getInstance().getConfig().getString("EconomyAccountNameTier" + tier);
        return economyAccountName;
    }

    public int getTier(Player p) {
        int tier = 0;
        if (p.hasPermission("FlyP.Fly.1")) {
            tier = 1;
        } else if (p.hasPermission("FlyP.Fly.2")) {
            tier = 2;
        } else if (p.hasPermission("FlyP.Fly.3")) {
            tier = 3;
        } else if (p.hasPermission("FlyP.Fly.4")) {
            tier = 4;
        } else if (p.hasPermission("FlyP.Fly.5")) {
            tier = 5;
        } else if (p.hasPermission("FlyP.Fly.6")) {
            tier = 6;
        } else if (p.hasPermission("FlyP.Fly.7")) {
            tier = 7;
        } else if (p.hasPermission("FlyP.Fly.8")) {
            tier = 8;
        } else if (p.hasPermission("FlyP.Fly.9")) {
            tier = 9;
        } else if (p.hasPermission("FlyP.Fly.10")) {
            tier = 10;
        } else if (p.hasPermission("FlyP.Fly.11")) {
            tier = 11;
        } else if (p.hasPermission("FlyP.Fly.12")) {
            tier = 12;
        }

        return tier;
    }

    public String getWithdrawTimer(int tier) {
        String withdrawTimer;
        withdrawTimer = Main.getInstance().getConfig().getString("WithdrawTimerTier" + tier);
        return withdrawTimer;
    }

    public int getFlyCheckTimer() {
        int flyCheckTimer;
        flyCheckTimer = Main.getInstance().getConfig().getInt("FlyCheckTimer");
        return flyCheckTimer;
    }

    public String getFlySpeed(int tier) {
        String flyCheckTimer;
        flyCheckTimer = Main.getInstance().getConfig().getString("FlySpeedTier" + tier);
        return flyCheckTimer;
    }

    public String getItemMetaTag(int tier) {
        String itemMetaTag;
        itemMetaTag = Main.getInstance().getConfig().getString("ItemMetaLoreTier" + tier).replace("&", "\u00A7");
        return itemMetaTag;
    }

    public boolean getFlyBanRequireTime() {

        boolean flyBanRequireTime = Main.getInstance().getConfig().getBoolean("FlyBanRequireTime");

        return flyBanRequireTime;
    }

    public int getFlyBanMaxTime() {

        int flyBanMaxTime = Main.getInstance().getConfig().getInt("FlyBanMaxTime");

        return flyBanMaxTime;
    }

    // **Messages

    public String getPlayerErrorConfigMessage() {

        String playerErrorMessageString = Main.getInstance().getConfig().getString("PlayerErrorMessage");

        return playerErrorMessageString;
    }

    public String getCommandUsageErrorConfigMessage() {

        String playerCommandUsageMessageString = Main.getInstance().getConfig().getString("CommandUsageErrorMessage");

        return playerCommandUsageMessageString;
    }

    public String getPermissionErrorConfigMessage() {

        String permissionErrorMessageString = Main.getInstance().getConfig().getString("PermissionErrorMessage");

        return permissionErrorMessageString;
    }

    public String getFlyingAlreadyEnabledConfigMessage() {

        String flyingAlreadyEnabledString = Main.getInstance().getConfig().getString("FlyingAlreadyEnabledMessage");

        return flyingAlreadyEnabledString;

    }

    public String getFlyingEnabledConfigMessage() {

        String flyingEnabledString = Main.getInstance().getConfig().getString("FlyingEnabledMessage");

        return flyingEnabledString;

    }

    public String getItemRequirementMessage() {

        String itemRequirementMessage = Main.getInstance().getConfig().getString("ItemRequirementMessage");

        return itemRequirementMessage;

    }

    public String getEXPRequirementMessage() {

        String EXPRequirementMessage = Main.getInstance().getConfig().getString("EXPRequirementMessage");

        return EXPRequirementMessage;

    }

    public String getMoneyRequiredMessage() {

        String moneyRequiredMessage = Main.getInstance().getConfig().getString("MoneyRequirementMessage");

        return moneyRequiredMessage;

    }

    public String getFlyingEnabledNoTimeLimitMessage() {

        String flyingEnabledNoTimeLimitMessage = Main.getInstance().getConfig().getString("FlyingEnabledNoTimeLimitMessage");

        return flyingEnabledNoTimeLimitMessage;

    }

    public String getInvalidArgumentMessage() {

        String invalidArgumentMessage = Main.getInstance().getConfig().getString("InvalidArgumentMessage");

        return invalidArgumentMessage;

    }

    public String getFlyingOffMessage() {

        String flyingOffMessage = Main.getInstance().getConfig().getString("FlyingOffIn10Message");

        return flyingOffMessage;

    }

    public String getItemRemovedMessage() {

        String itemRemovedMessage = Main.getInstance().getConfig().getString("ItemRemovedMessage");

        return itemRemovedMessage;

    }

    public String getEXPRemovedMessage() {

        String EXPRemovedMessage = Main.getInstance().getConfig().getString("EXPRemovedMessage");

        return EXPRemovedMessage;

    }

    public String getMoneyRemovedMessage() {

        String moneyRemovedMessage = Main.getInstance().getConfig().getString("MoneyRemovedMessage");

        return moneyRemovedMessage;

    }

    public String getTimeLeftMessage() {

        String timeLeftMessage = Main.getInstance().getConfig().getString("TimeLeftMessage");

        return timeLeftMessage;

    }

    public String getFlyingAlreadyDisabledMessage() {

        String flyingAlreadyDisabledMessage = Main.getInstance().getConfig().getString("FlyingAlreadyDisabledMessage");

        return flyingAlreadyDisabledMessage;

    }

    public String getFlyingDisabledMessage() {

        String flyingDisabledMessage = Main.getInstance().getConfig().getString("FlyingDisabledMessage");

        return flyingDisabledMessage;

    }

    public String getSecondsMessage() {

        String secondsMessage = Main.getInstance().getConfig().getString("SecondsMessage");

        return secondsMessage;

    }

    public String getMinutesMessage() {

        String minutesMessage = Main.getInstance().getConfig().getString("MinutesMessage");

        return minutesMessage;

    }

    public String getFlyingHasNoTimeLimitMessage() {

        String flyingHasNoTimeLimitMessage = Main.getInstance().getConfig().getString("FlyingHasNoTimeLimitMessage");

        return flyingHasNoTimeLimitMessage;

    }

    public String getTimeLeftNotFlyingMessage() {

        String timeLeftNotFlyingMessage = Main.getInstance().getConfig().getString("TimeLeftNotFlyingMessage");

        return timeLeftNotFlyingMessage;

    }

    public String getTimeLeftCommandMessage() {

        String timeLeftCommandMessage = Main.getInstance().getConfig().getString("TimeLeftCommandMessage");

        return timeLeftCommandMessage;

    }

    public String getRequiredBanTimeCommandMessage() {

        String requiredBanTimeCommandMessage = Main.getInstance().getConfig().getString("FlyBanRequiredTimeMessage");

        return requiredBanTimeCommandMessage;
    }

    public String getBanAddedMessage() {

        String getBanAddedMessage = Main.getInstance().getConfig().getString("BanAddedMessage");

        return getBanAddedMessage;
    }

    public String getMaxBanTimeReachedMessage() {

        String getMaxBanTimeReachedMessage = Main.getInstance().getConfig().getString("MaxBanTimeReachedMessage");

        return getMaxBanTimeReachedMessage;
    }

    public String getPlayerProtectedFromBanMessage() {

        String getPlayerProtectedFromBanMessage = Main.getInstance().getConfig().getString("PlayerProtectedFromBanMessage");

        return getPlayerProtectedFromBanMessage;
    }

    public String getPlayerOfflineMessage() {

        String getPlayerOfflineMessage = Main.getInstance().getConfig().getString("PlayerOfflineMessage");

        return getPlayerOfflineMessage;

    }

    public String getBanTimeValidationMessage() {

        String getBanTimeValidationMessage = Main.getInstance().getConfig().getString("BanTimeValidationMessage");

        return getBanTimeValidationMessage;
    }

    public String getCommandBanUsageErrorMessage() {

        String getCommandBanUsageErrorMessage = Main.getInstance().getConfig().getString("BanCommandUsageError");

        return getCommandBanUsageErrorMessage;
    }

    public String getPlayerNotBannedMessage() {

        String getPlayerNotBannedMessage = Main.getInstance().getConfig().getString("PlayerNotBannedMessage");

        return getPlayerNotBannedMessage;
    }

    public String getBanRemovedMessage() {

        String getBanRemovedMessage = Main.getInstance().getConfig().getString("BanRemovedMessage");

        return getBanRemovedMessage;
    }

    public String getPlayerBannedCheckMessage() {

        String getPlayerBannedCheckMessage = Main.getInstance().getConfig().getString("PlayerBannedCheckMessage");

        return getPlayerBannedCheckMessage;
    }

    public String getBannedPermanentlyMessage() {

        String getBannedPermanentlyMessage = Main.getInstance().getConfig().getString("BannedPermanentlyMessage");

        return getBannedPermanentlyMessage;
    }

    public String getItemRequirementLoreMessage() {

        String getItemRequirementLoreMessage = Main.getInstance().getConfig().getString("ItemRequirementLoreMessage");

        return getItemRequirementLoreMessage;
    }

    public String getPlayerBannedCantFlyMessage() {

        String getPlayerBannedCantFlyMessage = Main.getInstance().getConfig().getString("PlayerBannedCantFlyMessage");

        return getPlayerBannedCantFlyMessage;
    }

    public String getMessagePrefix() {

        String getMessagePrefix = Main.getInstance().getConfig().getString("MessagePrefix");

        return getMessagePrefix;
    }

    public String getBannedWorlds() {

        String getBannedWorlds = Main.getInstance().getConfig().getString("BannedWorlds");

        return getBannedWorlds;
    }

    public String getFreeFlightEnabled() {

        String getFreeFlightEnabled = Main.getInstance().getConfig().getString("FreeFlightEnabled");

        return getFreeFlightEnabled;
    }

    public String getFreeFlightEnabledMessage() {

        String getFreeFlightEnabledMessage = Main.getInstance().getConfig().getString("FreeFlightEnabledMessage");

        return getFreeFlightEnabledMessage;
    }

    public String getFreeFlightIsDisabledMessage() {

        String getFreeFlightEnabledMessage = Main.getInstance().getConfig().getString("FreeFlightIsDisabledMessage");

        return getFreeFlightEnabledMessage;
    }

    public String getFreeFlightEnabledPermanentlyMessage() {

        String getFreeFlightEnabledPermanentlyMessage = Main.getInstance().getConfig().getString("FreeFlightEnabledPermanentlyMessage");

        return getFreeFlightEnabledPermanentlyMessage;
    }

    public String getFreeFlightUsageErrorMessage() {

        String getFreeFlightUsageErrorMessage = Main.getInstance().getConfig().getString("FreeFlightUsageErrorMessage");

        return getFreeFlightUsageErrorMessage;
    }

    public String getFreeFlightEnabledTimeMessage() {

        String getFreeFlyEnabledTimeMessage = Main.getInstance().getConfig().getString("FreeFlightEnabledTimeMessage");

        return getFreeFlyEnabledTimeMessage;
    }

    public String getNotEnoughResourcesMessage() {

        String getNotEnoughResourcesMessage = Main.getInstance().getConfig().getString("NotEnoughResourcesMessage");

        return getNotEnoughResourcesMessage;
    }

    public String getFreeFlightDisabledMessage() {

        String getFreeFlightDisabledMessage = Main.getInstance().getConfig().getString("FreeFlightDisabledMessage");

        return getFreeFlightDisabledMessage;
    }

    public String getChargeOrderUsageErrorMessage() {

        String getChargeOrderUsageErrorMessage = Main.getInstance().getConfig().getString("ChargeOrderUsageErrorMessage");

        return getChargeOrderUsageErrorMessage;
    }

    public String getChargeOrderExpMessage() {

        String getChargeOrderExpMessage = Main.getInstance().getConfig().getString("ChargeOrderExpMessage");

        return getChargeOrderExpMessage;
    }

    public String getChargeOrderItemMessage() {

        return Main.getInstance().getConfig().getString("ChargeOrderItemMessage");
    }

    public int getFlyTempBanAmount() {
        // Gets temp ban time amount

        return Main.getInstance().getConfig().getInt("PvPTempBanTime");
    }

    public String getFlyTempBanMessage() {
        // TODO Auto-generated method stub
        return Main.getInstance().getConfig().getString("FlyTempBanMessage");
    }
}
