package com.ahellhound.bukkit.flypayment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PlayerInfo {

    private YamlConfiguration playerInfo = null;
    private File playerInfoConfigFile = null;

    // freefly
    public void reloadPlayerInfoConfig() {
        if (playerInfoConfigFile == null) {
            playerInfoConfigFile = new File(Main.getInstance().getDataFolder(), "playerInfo.yml");
        }

        playerInfo = YamlConfiguration.loadConfiguration(playerInfoConfigFile);

        // Look for defaults in the jar
        InputStream defConfigStream = Main.getInstance().getResource("playerInfo.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            playerInfo.setDefaults(defConfig);
        }
    }

    // gets banlist file
    public YamlConfiguration getPlayerInfoConfig() {
        if (playerInfo == null) {
            // reloads ban list
            reloadPlayerInfoConfig();
        }
        return playerInfo;
    }

    public void addEntryChargeOrder(Player p, String orderOne, String orderTwo) {
        int orderInt = 0;
        String orderOneString = orderOne.toLowerCase();
        String orderTwoString = orderTwo.toLowerCase();

        if (orderOneString.contains("exp")) {

            if (orderTwoString.contains("items")) {

                orderInt = 1;

            } else if (orderTwoString.contains("money")) {

                orderInt = 2;
            }

        } else if (orderOneString.contains("items")) {

            if (orderTwoString.contains("exp")) {
                orderInt = 3;

            } else if (orderTwoString.contains("money")) {

                orderInt = 4;
            }

        } else if (orderOneString.contains("money")) {

            if (orderTwoString.contains("exp")) {

                orderInt = 5;

            } else if (orderTwoString.contains("items")) {

                orderInt = 6;

            }

        }

        String pl = p.getName().toLowerCase();

        checkPlayerEntry(p);

        getPlayerInfoConfig().getConfigurationSection(pl).set("ChargeOrder", orderInt);
        savePlayerInfoConfig();

    }

    public int getChargeOrder(Player p) {
        String pl = p.getName().toLowerCase();

        checkPlayerEntry(p);

        return getPlayerInfoConfig().getConfigurationSection(pl).getInt("ChargeOrder");

    }

    public void checkPlayerEntry(Player p) {

        String pl = p.getName().toLowerCase();

        if (getPlayerInfoConfig().getConfigurationSection(pl) == null) {

            getPlayerInfoConfig().createSection(pl).set("ChargeOrder", 0);
            getPlayerInfoConfig().createSection(pl).set("TempFlyBanTime", 0);
            savePlayerInfoConfig();
        }

    }

    public void setFlyTempBan(Player p) {

        String pl = p.getName().toLowerCase();

        Configuration config = new Configuration();

        checkPlayerEntry(p);

        getPlayerInfoConfig().getConfigurationSection(pl).set("TempFlyBanTime",
                (config.getFlyTempBanAmount() * 1000) + System.currentTimeMillis());

        savePlayerInfoConfig();

    }

    public boolean isFlyTempBan(Player p) {

        String pl = p.getName().toLowerCase();

        checkPlayerEntry(p);

        if (getFlyTempBan(p) < System.currentTimeMillis()) {
            // sets player's time to 0, cleans player file
            getPlayerInfoConfig().getConfigurationSection(pl).set("TempFlyBanTime", 0);

            savePlayerInfoConfig();

            return false;

        } else {

            return true;

        }

    }

    public long getFlyTempBan(Player p) {
        String pl = p.getName().toLowerCase();

        checkPlayerEntry(p);

        return getPlayerInfoConfig().getConfigurationSection(pl).getLong("TempFlyBanTime");

    }

    // Saves ban list
    public void savePlayerInfoConfig() {
        if (playerInfo == null || playerInfoConfigFile == null) {
            return;
        }
        try {
            getPlayerInfoConfig().save(playerInfoConfigFile);
        } catch (IOException ex) {
            Main.getInstance().getLogger().log(Level.SEVERE, "Could not save configuration to " + playerInfoConfigFile, ex);
        }
    }

    // saves default ban list
    public void saveDefaultPlayerInfoConfig() {
        if (playerInfoConfigFile == null) {
            playerInfoConfigFile = new File(Main.getInstance().getDataFolder(), "playerInfo.yml");
        }
        if (!playerInfoConfigFile.exists()) {
            Main.getInstance().saveResource("playerInfo.yml", false);
        }
    }
}
