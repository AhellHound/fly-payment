package com.ahellhound.bukkit.flypayment;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerPayments {
    // Config constructor
    private Configuration config = new Configuration();
    private Messages Messages = new Messages();
    // flight constructor
    private Flight Flight = new Flight();

   /* public void withdrawItem(int tier, Player p) {
        // gets player order to remove from themselves
        // FileConfiguration.getWithdrawList()
        // TODO change to config w/ commands
        int withdrawOrder = PlayerInfo.getChargeOrder(p);
        // switches depending on removal order
        switch (withdrawOrder) {
        // withdraws exp, then item
        case 1:
            withdrawEXP(tier, p);
            withdrawItem(tier, p);
            withdrawMoney(tier, p);
            break;
            
        // withdraws item, then exp
        case 2:
            
            withdrawEXP(tier, p);
            withdrawItem(tier, p);
            withdrawMoney(tier, p);
            break;
            
        case 3:
            
            withdrawItem(tier, p);
            withdrawEXP(tier, p);
            withdrawMoney(tier, p);
            
        case 4:
            
            withdrawItem(tier, p);
            withdrawMoney(tier, p);
            withdrawEXP(tier, p);
            
        case 5:
            
            withdrawMoney(tier, p);
            withdrawEXP(tier, p);
            withdrawItem(tier, p);
            
        case 6:
            
            withdrawMoney(tier, p);
            withdrawItem(tier, p);
            withdrawEXP(tier, p);
            
        // if no case is found, this is default withdrawal order
        default:
            withdrawEXP(tier, p);
            withdrawItem(tier, p);
            withdrawMoney()
            break;

        }*/

    // withdraw exp method
    public void withdrawEXP(int tier, Player p) {
        // amount of exp to charge
        int chargeAmount = config.getExpChargeAmount(tier);
        // checks if player has enough exp
        if (chargeAmount == 0) {
            // doesn't do anything

        } else if (Utilities.getTotalExperience(p) < chargeAmount) {
            // disables players flight
            Flight.disableFlight(p, tier, 4);
        } else {
            // removes exp
            int expSet = Utilities.getTotalExperience(p) - (chargeAmount);

            Utilities.setTotalExperience(p, expSet);

            // sends player charge message
            Messages.EXPRemovedMessage(p, chargeAmount);
        }

    }

    public void chargeResources(Player p, int tier) {
        Configuration config = new Configuration();

        PlayerInfo PlayerInfo = new PlayerInfo();

        int order = PlayerInfo.getChargeOrder(p);

        if (order == 0) {
            if (expRequirementCheck(p, tier) && config.getExpChargeAmount(tier) != 0) {
                // withdraws exp
                withdrawEXP(tier, p);
                // TODO FIX ITEM REMOVAL AND CHECK
            } else if (itemRequirementCheck(p, tier) && config.getItemChargeAmount(tier) != 0) {
                // withdraws player item
                withdrawItem(tier, p);

            } else if (itemHasMeta(p, tier)) {

                Flight.disableFlight(p, tier, 4);
                p.sendMessage(Messages.itemRequirementLoreMessage(p, tier));

            } else {
                Flight.disableFlight(p, tier, 4);
                p.sendMessage(Messages.notEnoughResourcesMessage(p, tier));
            }

        } else {
            if (itemRequirementCheck(p, tier) && config.getItemChargeAmount(tier) != 0) {
                // withdraws exp
                withdrawItem(tier, p);
            } else if (itemHasMeta(p, tier)) {
                Flight.disableFlight(p, tier, 4);
                p.sendMessage(Messages.itemRequirementLoreMessage(p, tier));
            } else if (expRequirementCheck(p, tier) && config.getExpChargeAmount(tier) != 0) {
                withdrawEXP(tier, p);

            } else {
                Flight.disableFlight(p, tier, 4);
                p.sendMessage(Messages.notEnoughResourcesMessage(p, tier));
            }

        }

    }

    @SuppressWarnings("deprecation")
    public void withdrawItem(int tier, Player p) {
        // Config variables
        int itemChargeAmount = config.getItemChargeAmount(tier);
        // gets item name enum
        // String itemChargeEnum = config.getItemChargeEnum(tier);
        // gets inventory server
        PlayerInventory inventory = p.getInventory();
        // gets itemstack
        // ItemStack itemStack = new
        // ItemStack(Material.getMaterial(itemChargeEnum), itemChargeAmount);

        // checks if item charge is enabled
        if (itemChargeAmount == 0) {
            // returns true if no items are needed to fly

        } else if (config.getItemMetaTag(tier).length() == 2) {
            // checks if player has item and amount
            for (ItemStack is : inventory) {

                if (is == null || is.equals(Material.AIR)) {
                    continue;
                }

                if (is.getItemMeta().getLore() == null) {

                    continue;
                }

                if (itemChargeAmount > is.getAmount()) {

                    Flight.disableFlight(p, tier, 4);
                    continue;
                }
                if (is.getAmount() == itemChargeAmount) {
                    is.setAmount(0);
                    // updates player's inventory
                    p.getInventory().setContents(p.getInventory().getContents());
                    continue;
                }
                // remove's players item
                is.setAmount(is.getAmount() - itemChargeAmount);
                // updates player's inventory
                p.getInventory().setContents(p.getInventory().getContents());
                break;
            }

        } else {
            // iterates through player's inventory
            // int itemAmount = 0;
            for (ItemStack is : inventory) {

                if (is == null || is.equals(Material.AIR)) {
                    continue;
                }
                if (!is.hasItemMeta()) {
                    continue;
                }
                ItemMeta itemmeta = is.getItemMeta();
                List<String> lore = itemmeta.getLore();
                if (!lore.contains(config.getItemMetaTag(tier))) {
                    continue;
                }
                if (is.getAmount() == itemChargeAmount) {

                    is.setAmount(0);
                    // updates player's inventory
                    // p.getInventory().setContents(p.getInventory().getContents());
                    p.updateInventory();
                    continue;
                }
                if (itemChargeAmount > is.getAmount()) {
                    continue;
                }
                // remove's players item
                is.setAmount(is.getAmount() - itemChargeAmount);
                // updates player's inventory
                // p.getInventory().setContents(p.getInventory().getContents());
                p.updateInventory();
                break;
            }
        }
    }

    public boolean expRequirementCheck(Player p, int tier) {
        // gets config exp amount
        int expChargeAmount = config.getExpChargeAmount(tier);

        if (expChargeAmount == 0) {
            // returns true if exp removal is disabled
            return true;
        } else if (expChargeAmount > Utilities.getTotalExperience(p)) {
            // return false if player doesn't have enough exp
            return false;
        } else {
            // returns true if player has enough exp
            return true;
        }
    }

    public boolean itemRequirementCheck(Player p, int tier) {
        // Config variables
        int itemChargeAmount = config.getItemChargeAmount(tier);
        // gets item name enum
        String itemChargeEnum = config.getItemChargeEnum(tier);
        // gets inventory server
        PlayerInventory inventory = p.getInventory();
        // gets itemstack
        // ItemStack itemStack = new
        // ItemStack(Material.getMaterial(itemChargeEnum), itemChargeAmount);

        // checks if item charge is enabled
        if (itemChargeAmount == 0) {
            // returns true if no items are needed to fly
            return true;
        }
        // cehcks if item has enum
        if (config.getItemMetaTag(tier).length() == 2) {
            // checks if player has item and amount
            if (!inventory.contains(Material.valueOf(itemChargeEnum), itemChargeAmount)) {
                // returns false if player doesn't have items
                return false;
                // if player has item amount and type returns true
            } else {
                // if player has the requested inventory, returns true
                return true;
            }

        } else {
            // iterates through player's inventory
            int counter = 0;
            int itemAmount = 0;
            for (ItemStack is : inventory) {

                if (is == null || is.getItemMeta().getLore() == null) {
                    
                    continue;
                }

                if (is.equals(Material.AIR)) {

                    continue;
                }
                if (is.hasItemMeta()) {
                    ItemMeta itemmeta = is.getItemMeta();
                    List<String> lore = itemmeta.getLore();
                    // checks if lore is the same as config
                    if (lore.contains(config.getItemMetaTag(tier))) {

                        itemAmount = is.getAmount();

                        counter = counter + itemAmount;

                    }

                }
                
                
            }
            if (counter >= itemChargeAmount) {
                // returns true if inventory has the required lore item amount
                return true;

            } else {
                // returns false if not
                return false;
            }
        }
    }

    public boolean itemHasMeta(Player p, int tier) {
        // checks if an item has a meta tag
        if (config.getItemMetaTag(tier).contains("-1")) {
            // ends method
            return false;
        }

        // ends method
        return true;
    }

}
