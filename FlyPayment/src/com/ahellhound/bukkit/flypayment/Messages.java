package com.ahellhound.bukkit.flypayment;

import org.bukkit.entity.Player;

public class Messages {

    private Utilities Utilities = new Utilities();

    private Configuration Configuration = new Configuration();

    // /Messages
    public String playerErrorMessage() {
        // Gets message from config
        String playerErrorMessage = Configuration.getPlayerErrorConfigMessage();
        // returns message from config
        return playerErrorMessage;
    }

    public String commandUsageErrorMessage(Player p) {
        // Gets message from config
        String commandUsageErrorMessage = Configuration.getCommandUsageErrorConfigMessage();

        String commandUsageErrorMessageEnd = commandUsageErrorMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");

        return commandUsageErrorMessageEnd;
    }

    public String permissionErrorMessage(Player p) {
        // Gets message from config
        String permissionErrorMessage = Configuration.getPermissionErrorConfigMessage();

        String permissionErrorMessageEnd = permissionErrorMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns message from config
        return permissionErrorMessageEnd;
    }

    public String flyingAlreadyEnabledMessage(Player p) {
        // Gets message from config
        String flyingAlreadyEnabledMessage = Configuration.getFlyingAlreadyEnabledConfigMessage();
        // replaces %player% with command sender string name
        String flyingAlreadyEnabledMessageEnd = flyingAlreadyEnabledMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns message from config
        return flyingAlreadyEnabledMessageEnd;

    }

    public String flyingEnabledMessage(Player p) {
        // Gets message from config
        String flyingEnabledMessage = Configuration.getFlyingEnabledConfigMessage();

        String flyingEnabledMessageEnd = flyingEnabledMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("&", "\u00A7");
        // returns message from config
        return flyingEnabledMessageEnd;

    }

    public String itemRequirementMessage(Player p, int tier) {
        // gets item charge amount
        int itemChargeAmount = Configuration.getItemChargeAmount(tier);
        // gets item name
        String itemName = Configuration.getItemChargeEnum(tier);
        // Converts primitive int to string integer
        String itemChargeAmountString = Utilities.convertIntToString(itemChargeAmount);
        // Gets message from config
        String itemChargeAmountMessage = Configuration.getItemRequirementMessage();
        // replaces %player% with command sender string name; replaces
        // %ItemAmount% with item name required by tier set for command sender;
        // %ItemName% with item name required by tier set for command sender
        String itemChargeAmountMessageEnd = itemChargeAmountMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7")
                .replace("%ItemAmount%", itemChargeAmountString).replace("%ItemName%", itemName);
        // returns message from config
        return itemChargeAmountMessageEnd;

    }

    public String EXPRequirementMessage(Player p, int tier) {
        // gets chage amount of exp
        int expChargeAmount = Configuration.getExpChargeAmount(tier);
        // arithmetic for remaing exp needed
        int remainingEXPRequired = (expChargeAmount - p.getTotalExperience());
        // converts primitive int to string integer
        String RemainingEXP = Utilities.convertIntToString(remainingEXPRequired);
        // Gets message from config
        String EXPRequirementMessage = Configuration.getEXPRequirementMessage();
        // replaces %player% with command sender string name; replaces
        // %RemainingEXPNeeded% with exp needed left to make command sender fly
        String EXPRequirementMessageEnd = EXPRequirementMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%RemainingEXPNeeded%", RemainingEXP);
        // returns message from config
        return EXPRequirementMessageEnd;

    }

    public String moneyRequirementMessage(Player p, double moneyRequired, String moneyPlural, String moneySingular) {
        // converts primitive int to string integer
        String moneyRequiredString = Utilities.convertDoubleToString(moneyRequired);
        // Gets message from config s
        String moneyRequiredMessage = Configuration.getMoneyRequiredMessage();
        // replaces %player% with command sender string name; replaces
        // %RemainingEXPNeeded% with exp needed left to make command sender fly
        String moneyRequiredMessageEnd = moneyRequiredMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7")
                .replace("%RemainingMoneyNeeded%", moneyRequiredString).replace("%MoneyNamePlural%", moneyPlural)
                .replace("%MoneyNameSingular%", moneySingular);
        // returns message from config
        return moneyRequiredMessageEnd;

    }

    public String flyingEnabledNoTimelimitMessage(Player p) {
        // Gets message from config
        String flyingEnabledNoTimelimit = Configuration.getFlyingEnabledNoTimeLimitMessage();

        String flyingEnabledNoTimelimitEnd = flyingEnabledNoTimelimit.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns message from config
        return flyingEnabledNoTimelimitEnd;

    }

    public String invalidArgumentMessage(Player p) {
        // Gets message from config
        String invalidArgument = Configuration.getInvalidArgumentMessage();

        String invalidArgumentEnd = invalidArgument.replace("%player%", p.getName()).replace("%prefix%", Configuration.getMessagePrefix())
                .replace("&", "\u00A7");
        // returns message from config
        return invalidArgumentEnd;

    }

    public String flyingOffMessage(Player p) {
        // Gets message from config
        String flyingOffMessage = Configuration.getFlyingOffMessage();

        String flyingOffMessageEnd = flyingOffMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns message from config
        return flyingOffMessageEnd;

    }

    public String itemRemovedMessage(Player p, int tier) {
        // Gets message from config
        String itemRemovedMessage = Configuration.getItemRemovedMessage();
        // gets item charge amount
        int itemChargeAmount = Configuration.getItemChargeAmount(tier);
        String itemName = Configuration.getItemChargeEnum(tier);
        // gets item name // Converts int to String
        String itemChargeAmountString = Utilities.convertIntToString(itemChargeAmount);
        String itemRemovedMessageEnd = itemRemovedMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%ItemName%", itemName)
                .replace("%ItemChargeAmount%", itemChargeAmountString);
        // returns message from config
        return itemRemovedMessageEnd;

    }

    public String EXPRemovedMessage(Player p, int EXPChargeAmount) {
        // Gets message from config
        String EXPRemovedMessage = Configuration.getEXPRemovedMessage();
        // Converts int to String
        String EXPChargeAmountString = Utilities.convertIntToString(EXPChargeAmount);
        // replaces config holders
        String EXPRemovedMessageEnd = EXPRemovedMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7")
                .replace("%EXPChargeAmount%", EXPChargeAmountString);
        // returns message from config
        return EXPRemovedMessageEnd;

    }

    public String moneyRemovedMessage(Player p, int moneyChargeAmount, String moneyPlural, String moneySingular) {
        // Gets message from config
        String moneyRemovedMessage = Configuration.getMoneyRemovedMessage();
        // Converts int to String
        String moneyChargeAmountString = Utilities.convertIntToString(moneyChargeAmount);
        // replaces config holders
        String moneyRemovedMessageEnd = moneyRemovedMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7")
                .replace("%MoneyChargeAmount%", moneyChargeAmountString).replace("%MoneyNamePlural%", moneyPlural)
                .replace("%MoneyNameSingular%", moneySingular);
        // returns message from config
        return moneyRemovedMessageEnd;

    }

    public String timeLeftMessage(Player p, String timeLeft, String minutesOrSeconds) {
        // Gets message from config
        String timeLeftMessage = Configuration.getTimeLeftMessage();
        // replaces config holders
        String timeLeftMessageEnd = timeLeftMessage.replace("%player%", p.getName()).replace("%prefix%", Configuration.getMessagePrefix())
                .replace("&", "\u00A7").replace("%FlightTime%", timeLeft).replace("%MinutesOrSeconds%", minutesOrSeconds);
        // returns message from config
        return timeLeftMessageEnd;

    }

    public String flyingAlreadyDisabledMessage(Player p) {
        // Gets message from config
        String flyingAlreadyDisabledMessage = Configuration.getFlyingAlreadyDisabledMessage();

        String flyingAlreadyDisabledMessageEnd = flyingAlreadyDisabledMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns message from config
        return flyingAlreadyDisabledMessageEnd;

    }

    /**
     * *@param reason 0 = Fly Off; 1 = Free Fly Off; 2 = In PvP Zone; 3 =
     * Banned; 4 = No Resources Left; 5 = no build perms; 6 = get reason; 7 =
     * world flight; 8 = no combat reason;
     */
    public String flyingDisabledMessage(Player p, int reason) {
        // Gets message from config
        String flyingDisabledMessage = Configuration.getFlyingDisabledMessage();
        // flight constructor
        Flight Flight = new Flight();
        String stringReason;
        // if reason of disabling is due to more than one thing, get disable
        // reason will find out which it is
        if (reason == 6) {
            reason = Flight.getDisableReason(p);
        }
        // depending on the reason, the reason string in the message is changed
        switch (reason) {

        case (0):

            stringReason = "Flying Turned Off";
            break;

        case (1):
            stringReason = "Free-Fly has been turned off";
            break;

        case (2):
            stringReason = "You're in a PvP Zone";
            break;
        case (3):
            stringReason = "You've been banned from flying";
            break;
        case (4):
            stringReason = "You've ran out of resources to fly";
            break;
        case (5):
            stringReason = "You don't have permission to build in this area";
            break;
        case (7):
            stringReason = "You're not allowed to fly in this world!";
            break;
        case (8):
            stringReason = "You cannot engage in combat while flying!";
            break;
        default:
            stringReason = "Flying disabled";
            break;

        }

        // replaces string
        String flyingDisabledMessageEnd = flyingDisabledMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%Reason%", stringReason);
        // returns message from config
        return flyingDisabledMessageEnd;

    }

    public String secondsMessage() {
        // Gets message from config
        String secondsMessage = Configuration.getSecondsMessage();
        // returns secondsMessage
        return secondsMessage;

    }

    public String minutesMessage() {
        // Gets message from config
        String minutesMessage = Configuration.getMinutesMessage();
        // returns secondsMessage
        return minutesMessage;

    }

    public String timeLeftCommandMessage(Player p, String timeLeft) {
        // Gets message from config
        String timeLeftCommandMessage = Configuration.getTimeLeftCommandMessage();
        // replaces string
        String timeLeftCommandMessageEnd = timeLeftCommandMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%timeLeftCommand%", timeLeft);
        // returns secondsMessage
        return timeLeftCommandMessageEnd;

    }

    public String timeLeftNotFlyingMessage(Player p) {
        // Gets message from config
        String timeLeftNotFlyingMessage = Configuration.getTimeLeftNotFlyingMessage();
        // replaces string
        String timeLeftNotFlyingEnd = timeLeftNotFlyingMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return timeLeftNotFlyingEnd;

    }

    public String flyingHasNoTimeLimitMessage(Player p) {
        // Gets message from config
        String flyingHasNoTimeLimitMessage = Configuration.getFlyingHasNoTimeLimitMessage();
        // replaces string
        String flyingHasNoTimeLimitMessageEnd = flyingHasNoTimeLimitMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return flyingHasNoTimeLimitMessageEnd;

    }

    public String requiredBanTimeCommandMessage(Player p) {
        // Gets message from config
        String requiredBanTimeCommandMessage = Configuration.getRequiredBanTimeCommandMessage();
        // replaces string
        String requiredBanTimeCommandMessageEnd = requiredBanTimeCommandMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return requiredBanTimeCommandMessageEnd;

    }

    public String banAddedMessage(Player p, String bannedP, String timeRaw) {
        // Gets message from config
        String banAddedMessage = Configuration.getBanAddedMessage();
        // converts raw time to milliseconds
        long timeMilliseconds = Utilities.parseStringToMilliseconds(timeRaw);
        // Converts time to nice format
        String time = Utilities.parseLong(timeMilliseconds, false);
        // replaces string
        String banAddedMessageEnd = banAddedMessage.replace("%player%", p.getName()).replace("%prefix%", Configuration.getMessagePrefix())
                .replace("&", "\u00A7").replace("%BannedPlayerName%", bannedP).replace("%TimeBanned%", time);
        // returns secondsMessage
        return banAddedMessageEnd;

    }

    public String banAddedMessage(Player p, String bannedP, String timeRaw, boolean bool) {
        // Gets message from config
        String banAddedMessage = Configuration.getBanAddedMessage();
        // converts raw time to milliseconds
        long timeMilliseconds = Utilities.parseStringToMilliseconds(timeRaw);
        // Converts time to nice format
        String time = Utilities.parseLong(timeMilliseconds, false);
        if (timeRaw == "-1") {
            // permenent is added
            String banAddedMessageEnd = banAddedMessage.replace("%player%", p.getName())
                    .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%BannedPlayerName%", bannedP)
                    .replace("%TimeBanned%", "an infinite amount of time");
            // returns edited message for infinte ban time
            return banAddedMessageEnd;
        }
        if (timeMilliseconds == 0) {
            // sends player error message
            p.sendMessage(banTimeValidationMessage(p, timeRaw));

        }
        // replaces string
        String banAddedMessageEnd = banAddedMessage.replace("%player%", p.getName()).replace("%prefix%", Configuration.getMessagePrefix())
                .replace("&", "\u00A7").replace("%BannedPlayerName%", bannedP).replace("%TimeBanned%", time);

        // returns secondsMessage
        return banAddedMessageEnd;

    }

    public String maxBanTimeReachedMessage(Player p) {
        // Converts max ban time to string
        String maxBanTimeConverted = Utilities.parseLong((Configuration.getFlyBanMaxTime() * 1000), false);
        // Gets message from config
        String maxBanTimeReachedMessage = Configuration.getMaxBanTimeReachedMessage();
        // replaces string
        String maxBanTimeReachedMessageEnd = maxBanTimeReachedMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("&", "\u00A7")
                .replace("%MaxBanTime%", maxBanTimeConverted.toString());
        // returns secondsMessage
        return maxBanTimeReachedMessageEnd;

    }

    public String playerProtectedFromBanMessage(Player p, String protectedPlayer) {
        // Gets message from config
        String playerProtectedFromBanMessage = Configuration.getPlayerProtectedFromBanMessage();
        // replaces string
        String playerProtectedFromBanMessageEnd = playerProtectedFromBanMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%ProtectedPlayer%", protectedPlayer);
        // returns secondsMessage
        return playerProtectedFromBanMessageEnd;

    }

    public String playerOfflineMessage(String bannedPlayer, Player p) {
        // Gets message from config
        String playerOfflineMessage = Configuration.getPlayerOfflineMessage();
        // replaces string
        String playerOfflineMessageEnd = playerOfflineMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%BannedPlayerName%", bannedPlayer);
        // returns secondsMessage
        return playerOfflineMessageEnd;

    }

    public String banTimeValidationMessage(Player p, String incorrectEntry) {
        // Gets message from config
        String banTimeValidationMessage = Configuration.getBanTimeValidationMessage();
        // replaces string
        String banTimeValidationEnd = banTimeValidationMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%IncorrectEntry%", incorrectEntry);
        // returns secondsMessage
        return banTimeValidationEnd;

    }

    public String commandBanUsageErrorMessage(Player p) {
        // Gets message from config
        String commandBanUsageErrorMessage = Configuration.getCommandBanUsageErrorMessage();
        // replaces string
        String commandBanUsageErrorMessageEnd = commandBanUsageErrorMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return commandBanUsageErrorMessageEnd;

    }

    public String playerNotBannedMessage(Player p, String notBannedPlayer) {
        // Gets message from config
        String playerNotBannedMessage = Configuration.getPlayerNotBannedMessage();
        // replaces string
        String playerNotBannedMessageEnd = playerNotBannedMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%NotBannedPlayer%", notBannedPlayer);
        // returns secondsMessage
        return playerNotBannedMessageEnd;

    }

    public String banRemovedMessage(Player p, String removedPlayer) {
        // Gets message from config
        String banRemovedMessage = Configuration.getBanRemovedMessage();
        // replaces string
        String banRemovedMessageEnd = banRemovedMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%UnbannedPlayer%", removedPlayer);
        // returns secondsMessage
        return banRemovedMessageEnd;

    }

    public String playerBannedCheckMessage(Player p, String removedPlayer) {
        // Gets message from config
        String playerBannedCheckMessage = Configuration.getPlayerBannedCheckMessage();
        // banlist constructor
        BanList BanList = new BanList();
        // timed banned string
        Long timeBanned = BanList.getPlayerBanTime(removedPlayer);
        // checks if player is banned indefinitely
        // utilites constructor
        Utilities Utilites = new Utilities();
        long timeBannedLong = timeBanned;
        // banned time in string
        String timeBannedString = Utilites.parseLong(timeBannedLong, false);
        // replaces string
        String playerBannedCheckMessageEnd = playerBannedCheckMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%BannedPlayer%", removedPlayer)
                .replace("%TimeBannedFor%", timeBannedString);
        // returns message
        return playerBannedCheckMessageEnd;

    }

    public String playerBanCantFlyMessage(Player p) {
        // Gets message from config
        String playerBanCantFlyMessage = Configuration.getPlayerBannedCantFlyMessage();
        // banlist constructor
        BanList BanList = new BanList();
        // timed banned string
        Long timeBanned = BanList.getPlayerBanTime(p.getName());
        Utilities Utilites = new Utilities();
        long timeBannedLong = timeBanned;
        // Long timeBannedLong = Long.parseLong(timeBanned);
        // checks if player is banned indefinitely
        // banned time in string

        if (timeBanned.equals(-1)) {

            return playerBannedPermanentlyMessage(p, p.getName());
        }
        String timeBannedString = Utilites.parseLong(timeBannedLong + System.currentTimeMillis(), false);
        // replaces string
        String playerBanCantFlyMessageEnd = playerBanCantFlyMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%TimeBannedFor%", timeBannedString);
        // returns secondsMessage
        return playerBanCantFlyMessageEnd;
    }

    public String playerBannedPermanentlyMessage(Player p, String checkedPlayer) {
        // Gets message from config
        String playerBannedPermanentlyMessage = Configuration.getBannedPermanentlyMessage();
        // replaces string
        String playerBannedPermanentlyMessageEnd = playerBannedPermanentlyMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%BannedPlayer%", checkedPlayer);
        // returns secondsMessage
        return playerBannedPermanentlyMessageEnd;
    }

    public String itemRequirementLoreMessage(Player p, int tier) {
        // Gets message from config
        String itemRequirementLoreMessage = Configuration.getItemRequirementLoreMessage();
        // gets item name
        String itemName = Configuration.getItemChargeEnum(tier);
        // gets item charge amount
        String itemChargeAmountString = Integer.toString(Configuration.getItemChargeAmount(tier));
        // gets lore
        String lore = Configuration.getItemMetaTag(tier);
        // replaces string
        String itemRequirementLoreMessageEnd = itemRequirementLoreMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%Lore%", lore)
                .replace("%ItemAmount%", itemChargeAmountString).replace("%ItemName%", itemName);
        // returns secondsMessage
        return itemRequirementLoreMessageEnd;
    }

    public String freeFlightEnabled(Player p) {
        // Gets message from config
        String playerBannedPermanentlyMessage = Configuration.getFreeFlightEnabledMessage();
        // replaces string
        String playerBannedPermanentlyMessageEnd = playerBannedPermanentlyMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return playerBannedPermanentlyMessageEnd;
    }

    public String freeFlightIsDisabledMessage(Player p) {
        // Gets message from config
        String freeFlightIsDisabledMessage = Configuration.getFreeFlightIsDisabledMessage();
        // replaces string
        String freeFlightIsDisabledMessageEnd = freeFlightIsDisabledMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return freeFlightIsDisabledMessageEnd;
    }

    public String freeFlightDisabledMessage(Player p) {
        // Gets message from config
        String freeFlightDisabledMessage = Configuration.getFreeFlightDisabledMessage();
        // replaces string
        String freeFlightDisabledMessageEnd = freeFlightDisabledMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return freeFlightDisabledMessageEnd;
    }

    public String freeFlightEnabledPermanentlyMessage(Player p) {
        // Gets message from config
        String freeFlightEnabledPermanentlyMessage = Configuration.getFreeFlightEnabledPermanentlyMessage();
        // replaces string
        String freeFlightEnabledPermanentlyMessageEnd = freeFlightEnabledPermanentlyMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return freeFlightEnabledPermanentlyMessageEnd;
    }

    public String freeFlightEnabledTimeMessage(Player p, long timeInMilliseconds) {
        // Gets message from config
        String freeFlightEnabledPermanentlyMessage = Configuration.getFreeFlightEnabledTimeMessage();
        String parsedTimeInMilliseconds = Utilities.parseLong(timeInMilliseconds, false);
        // replaces string
        String freeFlightEnabledPermanentlyMessageEnd = freeFlightEnabledPermanentlyMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7")
                .replace("%FreeFlyTime%", parsedTimeInMilliseconds);
        // returns secondsMessage
        return freeFlightEnabledPermanentlyMessageEnd;
    }

    public String freeFlightUsageErrorMessage(Player p) {
        // Gets message from config
        String getFreeFlightUsageErrorMessage = Configuration.getFreeFlightUsageErrorMessage();
        // replaces string
        String getFreeFlightUsageErrorMessageEnd = getFreeFlightUsageErrorMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return getFreeFlightUsageErrorMessageEnd;
    }

    public String notEnoughResourcesMessage(Player p, int tier) {
        // Gets message from config
        String getNotEnoughResourcesMessage = Configuration.getNotEnoughResourcesMessage();
        int EXPChargeAmount = Configuration.getExpChargeAmount(tier);
        String stringEXPAmount = Utilities.convertIntToString(EXPChargeAmount);
        int itemChargeAmount = Configuration.getItemChargeAmount(tier);
        String stringItemAmount = Utilities.convertIntToString(itemChargeAmount);
        String stringItemNeeded = Configuration.getItemChargeEnum(tier);

        // replaces string
        String getNotEnoughResourcesMessageEnd = getNotEnoughResourcesMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7").replace("%ItemsNeeded%", stringItemNeeded)
                .replace("%EXPAmount%", stringEXPAmount).replace("%ItemAmount%", stringItemAmount);
        // returns secondsMessage
        return getNotEnoughResourcesMessageEnd;
    }

    public String chargeOrderUsageErrorMessage(Player p) {
        // Gets message from config
        String getChargeOrderUsageErrorMessage = Configuration.getChargeOrderUsageErrorMessage();
        // replaces string
        String getChargeOrderUsageErrorMessageEnd = getChargeOrderUsageErrorMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return getChargeOrderUsageErrorMessageEnd;
    }

    public String chargeOrderExpMessage(Player p) {
        // Gets message from config
        String chargeOrderExpMessage = Configuration.getChargeOrderExpMessage();
        // replaces string
        String chargeOrderExpMessageEnd = chargeOrderExpMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return chargeOrderExpMessageEnd;
    }

    public String chargeOrderItemMessage(Player p) {
        // Gets message from config
        String getChargeOrderItemMessage = Configuration.getChargeOrderItemMessage();
        // replaces string
        String getChargeOrderItemMessageEnd = getChargeOrderItemMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return getChargeOrderItemMessageEnd;
    }

    public String flyTempBan(Player p) {
        // Gets message from config
        String getChargeOrderItemMessage = Configuration.getFlyTempBanMessage();
        // replaces string
        String getChargeOrderItemMessageEnd = getChargeOrderItemMessage.replace("%player%", p.getName())
                .replace("%prefix%", Configuration.getMessagePrefix()).replace("&", "\u00A7");
        // returns secondsMessage
        return getChargeOrderItemMessageEnd;
    }

}
