package com.ahellhound.bukkit.flypayment;

import java.util.logging.Logger;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
    Logger log = Logger.getLogger("Minecraft");

    // eliminate Variables
    public static Permission permission = null;
    // Config instance variable
    private static Main instance;
    // Reload config instance variable
    private static Main reloadInstance;
    // Messages constructor
    private Messages Messages = new Messages();

    @Override
    public void onEnable() {
        // Free Fly construcotor
        FreeFly FreeFly = new FreeFly();
        // Scheduler constructor
        Scheduler Scheduler = new Scheduler();
        // Ban list constructor
        BanList BanList = new BanList();
        // Player Info constructor
        PlayerInfo PlayerInfo = new PlayerInfo();
        // Config instance
        instance = this;
        // reloads config
        reloadConfiguration();
        // reloads ban list
        BanList.reloadBanListConfig();
        // reloads free fly config
        FreeFly.reloadFreeFlyConfig();
        // reploads player info
        PlayerInfo.reloadPlayerInfoConfig();
        // Starts timers
        Scheduler.enableWithdrawTimer();
        Scheduler.enableFreeFlyTimer();

        // checks permission
        if (!setupPermissions()) {
            log.severe(String.format("[%s] - Disabled! You NEED Vault, with a Permission plugin!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // saves default config
        saveDefaultConfig();
        // saves banlist
        BanList.saveDefaultBanList();
        // saves free fly file
        FreeFly.saveDefaultFreeFlyConfig();
        // saves default
        PlayerInfo.saveDefaultPlayerInfoConfig();
        // gets plugin manager
        getServer().getPluginManager().registerEvents(this, this);
        
        log.info("[" + getDescription().getName() + "] " + getDescription().getName() + " version " + getDescription().getVersion()
                + " is now enabled.");
        log.info("[" + getDescription().getName() + "]" + " Made By AhellHound");

    }

    // Main instance
    public static Main getInstance() {
        return instance;

    }

    // Reload instance
    public static Main getReloadInstance() {
        return reloadInstance;

    }

    // Reloads configuration, creates tiers
    public void reloadConfiguration() {
        reloadConfig();
        reloadInstance = this;

    }

    // Hooks into Vault
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(
                net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        // Player check
        if (!(sender instanceof Player)) {
            // sends console message
            sender.sendMessage(Messages.playerErrorMessage());
            // ends method
            return true;
        }

        // player sender
        Player p = (Player) sender;
        // Confifuration constructor
        Configuration Configuration = new Configuration();
        // tier variable
        int tier = Configuration.getTier(p);
        // if permission node isn't within the 1-12 range
        if (tier == 0) {
            // sends player message
            p.sendMessage(Messages.permissionErrorMessage(p));
            // method return
            return true;
        }

        // checks if fly on has only one argument
        if (args.length == 0) {

            // sends player command usage error message
            p.sendMessage(Messages.commandUsageErrorMessage(p));
            // ends method
            return true;

        }
        // Arg's check

        // Checks to see if 'Flying ' is enabled
        if (args[0].equalsIgnoreCase("on")) {
            // Ban list constructor
            BanList BanList = new BanList();

            if (BanList.isBannedFly(p)) {
                // sends player message about ban
                p.sendMessage(Messages.playerBanCantFlyMessage(p));

                return true;
            }

            // checks if player can fly
            if (p.getAllowFlight()) {
                // sends player message
                p.sendMessage(Messages.flyingAlreadyEnabledMessage(p));
                // returns true
                return true;
            }

            // Free Fly construcotor
            FreeFly FreeFly = new FreeFly();

            if (FreeFly.isFreeFlyTimerEnabled()) {

                // Flight constructor
                Flight Flight = new Flight();
                // enables free flight
                Flight.enableFreeFlight(p);
                return true;
            }
            // TODO fix
            // If player has flyp.bypass, gives them fly without checks TEST
            if (p.hasPermission("flyp.bypass")) {
                // Scheduler constructor
                Scheduler Scheduler = new Scheduler();
                // sends player message about flying
                p.sendMessage(Messages.flyingEnabledMessage(p));
                // enables flight for no charge
                Timers Timers = new Timers();
                p.setAllowFlight(true);
                p.setFlying(true);
                Timers.addFlyCheckArray(p);
                Scheduler.addSafePlayer(p);
                // TODO make method
                // ends method
                return true;

            }

            if (!p.getAllowFlight()) {
                // TODO add an estimated time w/ items and exp in inventory
                p.sendMessage(Messages.flyingEnabledMessage(p));
                // Flight constructor
                Flight Flight = new Flight();
                // enables flight
                Flight.enableFlight(p, tier);
                // sends player message about flight
                return true;
            }
        }

        // The 'off' argument
        else if (args[0].equalsIgnoreCase("off")) {

            if (!p.getAllowFlight()) {
                // sends player message saying flights already enabled
                p.sendMessage(Messages.flyingAlreadyDisabledMessage(p));
                return true;
            }

            Timers Timers = new Timers();

            if (Timers.isInFreeFlyArray(p)) {
                // Flight constructor
                Flight Flight = new Flight();
                Flight.disableFreeFlight(p);
                return true;

            }
            // Flight constructor
            Flight Flight = new Flight();
            // ends flight
            Flight.disableFlight(p, tier, 0);
            // Reload argument

        } else if (args[0].equalsIgnoreCase("disableAll")) {
            // permission check
            if (!p.hasPermission("flyp.disableall")) {
                p.sendMessage(Messages.permissionErrorMessage(p));
                return true;
            }
            // Scheduler constructor
            Scheduler Scheduler = new Scheduler();
            // TODO
            // Disables everyone's flight
            Scheduler.disableAllPlayersFlight();

        } else if (args[0].equals("chargeorder")) {

            if (args.length == 2) {
                p.sendMessage(Messages.chargeOrderUsageErrorMessage(p));
                return true;
            }

            if (args[1].equalsIgnoreCase("exp")) {

                if (args[2].equalsIgnoreCase("items")) {
                    // Player Info constructor
                    PlayerInfo PlayerInfo = new PlayerInfo();
                    p.sendMessage(Messages.chargeOrderExpMessage(p));
                    PlayerInfo.addEntryChargeOrder(p, args[1], args[2]);
                    return true;

                } else if (args[2].equalsIgnoreCase("money")) {
                    // Player Info constructor
                    PlayerInfo PlayerInfo = new PlayerInfo();
                    p.sendMessage(Messages.chargeOrderExpMessage(p));
                    PlayerInfo.addEntryChargeOrder(p, args[1], args[2]);
                    return true;
                } else {

                    p.sendMessage(Messages.chargeOrderUsageErrorMessage(p));
                    return true;
                }

            } else if (args[1].equalsIgnoreCase("items")) {

                if (args[2].equalsIgnoreCase("exp")) {

                    // Player Info constructor
                    PlayerInfo PlayerInfo = new PlayerInfo();
                    p.sendMessage(Messages.chargeOrderExpMessage(p));
                    PlayerInfo.addEntryChargeOrder(p, args[1], args[2]);
                    return true;

                } else if (args[2].equalsIgnoreCase("money")) {

                    // Player Info constructor
                    PlayerInfo PlayerInfo = new PlayerInfo();
                    p.sendMessage(Messages.chargeOrderExpMessage(p));
                    PlayerInfo.addEntryChargeOrder(p, args[1], args[2]);
                    return true;

                } else {

                    p.sendMessage(Messages.chargeOrderUsageErrorMessage(p));
                    return true;

                }

            } else if (args[1].equalsIgnoreCase("money")) {

                if (args[2].equalsIgnoreCase("items")) {
                    // Player Info constructor
                    PlayerInfo PlayerInfo = new PlayerInfo();
                    p.sendMessage(Messages.chargeOrderExpMessage(p));
                    PlayerInfo.addEntryChargeOrder(p, args[1], args[2]);
                    return true;

                } else if (args[2].equalsIgnoreCase("exp")) {
                    // Player Info constructor
                    PlayerInfo PlayerInfo = new PlayerInfo();
                    p.sendMessage(Messages.chargeOrderExpMessage(p));
                    PlayerInfo.addEntryChargeOrder(p, args[1], args[2]);
                    return true;

                } else {

                    p.sendMessage(Messages.chargeOrderUsageErrorMessage(p));
                    return true;

                }

            } else {

                p.sendMessage(Messages.chargeOrderUsageErrorMessage(p));
                return true;
            }
        } else if (args[0].equals("freefly")) {

            if (args.length == 1) {
                p.sendMessage(Messages.freeFlightUsageErrorMessage(p));
                return true;
            }

            // permission check
            if (!(p.hasPermission("FlyP.freefly"))) {
                // Sends player no permission message
                p.sendMessage(Messages.permissionErrorMessage(p));
                // returns true
                return true;

            }

            if (args[1].equalsIgnoreCase("off")) {
                Timers Timers = new Timers();

                // check if free fly is on
                if (Timers.isInFreeFlyArray(p)) {
                    // Free Fly construcotor
                    FreeFly FreeFly = new FreeFly();
                    // Disables free fly
                    FreeFly.disableFreeFlightConfig();

                    p.sendMessage(Messages.freeFlightDisabledMessage(p));

                    return true;

                } else {
                    // messages user flying enabled

                    p.sendMessage(Messages.freeFlightIsDisabledMessage(p));
                    return true;
                }

            } else if (args[1].equalsIgnoreCase("on")) {

                // Configuration constructor
                Utilities Utilities = new Utilities();

                if (args.length == 2) {
                    // Free Fly construcotor
                    FreeFly FreeFly = new FreeFly();
                    // TODO Fix file saving
                    // sets free fly to true
                    FreeFly.setFreeFly(true);
                    // sets to -1 for unlimited
                    FreeFly.setFreeFlyTime(-1);
                    // sends player message
                    p.sendMessage(Messages.freeFlightEnabledPermanentlyMessage(p));
                    // ends method
                    return true;

                }
                // converts time arg to string
                String timeFreeFly = args[2];

                // Converts time banned to milliseconds
                long timeFreeFlyMilliseconds = Utilities.parseStringToMilliseconds(timeFreeFly);
                // checks if player entered incorrect time format
                if (timeFreeFlyMilliseconds == 0) {
                    // sends player message if ban was incorrect
                    p.sendMessage(Messages.banTimeValidationMessage(p, timeFreeFly));
                    // ends method
                    return true;
                } else {
                    // Free Fly construcotor
                    FreeFly FreeFly = new FreeFly();
                    // Scheduler constructor
                    Scheduler Scheduler = new Scheduler();
                    // enables free flight
                    FreeFly.enableFreeFlyConfig(timeFreeFly);
                    Scheduler.enableFreeFlyTimer();
                    p.sendMessage(Messages.freeFlightEnabledTimeMessage(p, timeFreeFlyMilliseconds));
                    return true;
                }

            } else {
                // sends player invalid args message
                p.sendMessage(Messages.freeFlightUsageErrorMessage(p));
                return true;
            }

        } else if (args[0].equalsIgnoreCase("ban")) {

            if (!(p.hasPermission("FlyP.ban.add"))) {
                // Sends player no permission message
                p.sendMessage(Messages.permissionErrorMessage(p));
                // returns true
                return true;
            }

            // checks if ban is lacking arguments
            if ((args.length == 1) || (args.length == 4)) {
                // sends player command usage error message
                p.sendMessage(Messages.commandBanUsageErrorMessage(p));
                // ends method
                return true;

            }

            // Arg amount check
            if (args.length == 3) {
                // converts banned player argument to string
                String bannedPlayer = args[1];

                // checks if player is exempt from being banned

                try {
                    if (getServer().getPlayer(bannedPlayer).hasPermission("FlyP.ban.protect")) {
                        // tells command sender player was not banned because
                        // protected from fly ban
                        p.sendMessage(Messages.playerProtectedFromBanMessage(p, bannedPlayer));
                        // ends method
                        return true;
                    }
                } catch (NullPointerException e) {

                }
                // Configuration constructor
                Utilities Utilities = new Utilities();
                // converts time arg to string
                String timeBanned = args[2];
                // Converts time banned to milliseconds
                long timeBannedMilliseconds = Utilities.parseStringToMilliseconds(timeBanned);
                // checks if player entered incorrect time format
                if (timeBannedMilliseconds == 0) {
                    // sends player message if ban was incorrect
                    p.sendMessage(Messages.banTimeValidationMessage(p, timeBanned));
                    // ends method
                    return true;
                }
                // Ban list constructor
                BanList BanList = new BanList();
                // checks max ban time
                if (BanList.hasMaxBanTime()) {
                    // checks if command sender has override command to bypass
                    // max ban time
                    if (p.hasPermission("FlyP.ban.override")) {
                        // adds player to ban list
                        BanList.addPlayerBanList(bannedPlayer, timeBanned);
                        // Sends player message saying person was banned
                        p.sendMessage(Messages.banAddedMessage(p, bannedPlayer, timeBanned));
                        // ends method
                        return true;
                    }

                    // checks if ban is longer than max time
                    if (timeBannedMilliseconds > BanList.getMaxBanTime()) {
                        // sends player message saying ban was too long
                        p.sendMessage(Messages.maxBanTimeReachedMessage(p));
                        // returns true, ends method
                        return true;
                        // if ban time is within max ban time limit, runs ban
                        // list add
                    } else {

                        // adds player to ban list
                        BanList.addPlayerBanList(bannedPlayer, timeBanned);
                        // Sends player message saying person was banned
                        p.sendMessage(Messages.banAddedMessage(p, bannedPlayer, timeBanned));
                        // ends method
                        return true;
                    }

                    // if no max ban, then runs ban list add
                } else {

                    // adds player to ban list
                    BanList.addPlayerBanList(bannedPlayer, timeBanned);
                    // Sends player message saying person was banned
                    p.sendMessage(Messages.banAddedMessage(p, bannedPlayer, timeBanned));
                    // ends method
                    return true;

                }

                // doesn't include time
            } else if (args.length == 2) {
                // Ban list constructor
                BanList BanList = new BanList();
                // checks if command sender has override pemrission to bypass
                // max ban time
                if (p.hasPermission("FlyP.ban.override")) {
                    // banned player
                    String bannedPlayer = args[1];
                    // time banned permanent
                    String timeBanned = ("-1");
                    // adds player to ban list
                    BanList.addPlayerBanList(bannedPlayer, timeBanned);
                    // Sends player message saying person was banned
                    p.sendMessage(Messages.banAddedMessage(p, bannedPlayer, timeBanned, true));
                    // ends method
                    return true;
                }

                // checks if player needs to enter time
                if (BanList.getRequireBanTime()) {

                    // sends player message requiring to set a ban time
                    p.sendMessage(Messages.requiredBanTimeCommandMessage(p));
                    // ends method
                    return true;

                } else {

                    // banned player
                    String bannedPlayer = args[1];
                    // time banned permanent
                    String timeBanned = ("-1");
                    // adds player to ban list
                    BanList.addPlayerBanList(bannedPlayer, timeBanned);
                    // Sends player message saying person was banned
                    p.sendMessage(Messages.banAddedMessage(p, bannedPlayer, timeBanned, true));
                    // ends method
                    return true;

                }

            }
        } else if (args[0].equalsIgnoreCase("removeban")) {
            // checks if player has ban removal permission
            if (!p.hasPermission("FlyP.ban.remove")) {
                // Sends player no permission message
                p.sendMessage(Messages.permissionErrorMessage(p));
                // returns true
                return true;
            }
            // checks if incorrect amount of args are given
            if (args.length >= 3 || args.length < 2) {
                // sends player command usage error emssage
                p.sendMessage(Messages.commandBanUsageErrorMessage(p));
                // ends method
                return true;

            }
            // assigns variable to player being removed
            String removedPlayer = args[1];
            // Ban list constructor
            BanList BanList = new BanList();
            // checks if player is banned
            if (BanList.isBannedFly(removedPlayer)) {
                // removes player ban
                BanList.removeBan(removedPlayer);
                // sends ban removed message
                p.sendMessage(Messages.banRemovedMessage(p, removedPlayer));
                return true;

            } else {
                // Sends command sender message player was not banned to begin
                // with
                p.sendMessage(Messages.playerNotBannedMessage(p, removedPlayer));
                return true;
            }

            // end of removeban command
        } else if (args[0].equalsIgnoreCase("checkban")) {
            // checks if player has ban removal permission
            if (!p.hasPermission("FlyP.ban.check")) {
                // Sends player no permission message
                p.sendMessage(Messages.permissionErrorMessage(p));
                // returns true
                return true;
            }
            // checks if incorrect amount of args are given
            if (args.length >= 3 || args.length < 2) {
                // sends player command usage error message
                p.sendMessage(Messages.commandBanUsageErrorMessage(p));
                // ends method
                return true;

            }
            // assigns player to variable
            String checkedPlayer = args[1];
            // Ban list constructor
            BanList BanList = new BanList();
            // checks ban
            if (BanList.isBannedFly(checkedPlayer)) {
                // checks if player is permanently banned
                if (BanList.getPlayerBanTime(checkedPlayer).equals(-1)) {

                    // sends command sender different message about permanent
                    // ban
                    p.sendMessage(Messages.playerBannedPermanentlyMessage(p, checkedPlayer));
                    return true;
                }
                // Sends command sender message about player's ban and their
                // time
                p.sendMessage(Messages.playerBannedCheckMessage(p, checkedPlayer));
                return true;
            } else {
                // sends command to sender saying player is not banned
                p.sendMessage(Messages.playerNotBannedMessage(p, checkedPlayer));
                return true;
            }
        } else {
            p.sendMessage(Messages.invalidArgumentMessage(p));
            return true;
        }

        // Method return
        return true;
    }

}
