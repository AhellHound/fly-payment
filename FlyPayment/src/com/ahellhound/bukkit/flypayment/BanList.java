package com.ahellhound.bukkit.flypayment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class BanList {
    // configuration constructor
    private Configuration Configuration = new Configuration();
    // Utilities constructor
    Utilities Utilities = new Utilities();

    public void addPlayerBanList(String p, String rawTime) {
        // converts string to long
        long timeFormatted = Utilities.parseStringToMilliseconds(rawTime);
        // checks if player was banned indefinitely
        if (rawTime == "-1") {
            // changes time formatted string to stay '-1'
            int timeFormattedString = -1;
            // creates player name in file
            getBanList().getConfigurationSection("Bans").set(p.toLowerCase(), timeFormattedString);
            // Reloads ban list
            saveBanList();
        } else {
            // converts long to string
            // String timeFormattedString =
            // Utilities.convertLongToString(timeFormatted);
            // creates player name in file
            if (getBanList().getConfigurationSection("Bans") == null) {

                getBanList().createSection("Bans");

            }

            getBanList().getConfigurationSection("Bans").set(p.toLowerCase(), timeFormatted + System.currentTimeMillis());
            // Saves ban list
            saveBanList();
        }

    }

    public boolean isBannedFly(Player p) {
        // updates ban list
        updateBanList();
        // Checks file for player name
        if (getBanList().getConfigurationSection("Bans").getString(p.getName().toLowerCase()) == null) {

            return false;
        } else {

            return true;
        }
    }

    public boolean isBannedFly(String p) {
        // updates ban list
        updateBanList();
        String configBanList = getBanList().getConfigurationSection("Bans").getString(p.toLowerCase());
        // Checks file for player name
        if (configBanList == null) {
            // returns true if banned
            return false;
        }
        // returns false if player not banned
        return true;
    }

    public void updateBanList() {

        // puts all the player names into a string set
        Set<String> playerString = getBanList().getConfigurationSection("Bans").getKeys(false);
        // current time
        long currentTime = System.currentTimeMillis();
        // iterates through the whole set, looking for players with a time thats
        // equal to or above the current time. If so, removes ban
        for (String playerName : playerString) {
            // checks if the player's time is less than or equal to current
            // time, and if player is permanently banned (-1)

            if ((!getBanList().getConfigurationSection("Bans").getString(playerName).contains("-1"))
                    && getBanList().getConfigurationSection("Bans").getLong(playerName) <= currentTime) {
                // removes player ban
                removeBan(playerName);

                saveBanList();

            }

        }

    }

    public void removeBan(String bannedPlayer) {

        getBanList().getConfigurationSection("Bans").set(bannedPlayer, null);
        // Saves ban list
        saveBanList();

    }

    public boolean getRequireBanTime() {
        // checks in config if ban time is required
        if (!Configuration.getFlyBanRequireTime()) {
            // returns false if false is entered in config
            return false;
        }
        // returns true if ban is required
        return true;
    }

    public boolean hasMaxBanTime() {
        // checks if config has max ban time, 0 is false
        if (Configuration.getFlyBanMaxTime() == 0) {
            // returns true
            return false;

        }
        // returns false if no max ban time
        return true;
    }

    public int getMaxBanTime() {
        // gets seconds for ban time
        int banTime = Configuration.getFlyBanMaxTime();
        // converts seconds to milliseconds
        int banTimeMilliseconds = (banTime * 1000);
        // returns ban time
        return banTimeMilliseconds;
    }

    public Long getPlayerBanTime(String p) {
        updateBanList();
        // gets ban time set in config
        int banTimeInt = getBanList().getConfigurationSection("Bans").getInt(p);
        
        // checks if player is permanently ban     
        if (banTimeInt == -1){
            
            return -1L;
        }
        // parses string
        //Long parsedTime = Long.;
        // correctly formats time
        Long banTime = getBanList().getConfigurationSection("Bans").getLong(p);
        //Long parsedTimeLong = banTime;
        // tostring
        //String banTimeString = String.valueOf(banTime - System.currentTimeMillis());
        Long banTimeParsed = banTime - System.currentTimeMillis();
        // returns ban time
        return banTimeParsed;
    }

    public boolean isInBannedWorld(Player p) {
        String bannedWorlds = Configuration.getBannedWorlds();
        ArrayList<String> bannedWorldsArray = new ArrayList<String>(Arrays.asList(bannedWorlds.split(" ")));
        String playerWorld = p.getWorld().getName();
        if (bannedWorldsArray.contains(playerWorld)) {
            // returns true
            return true;
        }

        // returns false
        return false;
    }

    public void reloadBanList() {
        // reloads ban list
        reloadBanListConfig();
    }
    
    //File configuration methods
    private YamlConfiguration banList = null;
    private File banListConfigFile = null;
    //Loads banlist config if not present
    public void reloadBanListConfig() {
        if (banListConfigFile == null) {
            banListConfigFile = new File(Main.getInstance().getDataFolder(), "banList.yml");
        }
        banList = YamlConfiguration.loadConfiguration(banListConfigFile);
     
        // Look for defaults in the jar
        InputStream defConfigStream = Main.getInstance().getResource("banList.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            banList.setDefaults(defConfig);
        }
    }
    
    //gets banlist file
    public YamlConfiguration getBanList() {
        if (banList == null) {
            //reloads ban list
            reloadBanListConfig();
        }
        return banList;
    }
    
    //Saves ban list
    public void saveBanList() {
        if (banList == null || banListConfigFile == null) {
            return;
        }try {
            getBanList().save(banListConfigFile);
        } catch (IOException ex) {
            Main.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + banListConfigFile, ex);
        }
    }

    //saves default ban list
    public void saveDefaultBanList() {
        if (banListConfigFile == null) {
            banListConfigFile = new File(Main.getInstance().getDataFolder(), "banList.yml");
        }
        if (!banListConfigFile.exists()) {            
             Main.getInstance().saveResource("banList.yml", false);
         }
    }

}
