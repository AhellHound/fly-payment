package com.ahellhound.bukkit.flypayment;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventHandlers implements Listener{
    // Scheduler constructor
    private Scheduler Scheduler = new Scheduler();
    // Configuration constructor
    private Configuration Configuration = new Configuration();
    // RiseMC Timers
    Timers Timers = new Timers();
    // Flight constructor
    Flight Flight = new Flight();
    // Configuration constructor
    Configuration config = new Configuration();
    Timers timers = new Timers();
    Messages Messages = new Messages();
    PlayerInfo PlayerInfo = new PlayerInfo();
   

    // Quit event
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        int tier = Configuration.getTier(p);
        timers.removeFromArrays(p);
        timers.removeWithdrawArray(p, tier);
    }

    public void onPlayerDamageGivenEvent(EntityDamageEvent event) {

        if (event.getEntity() instanceof Player) {

            Player p = ((Player) event.getEntity());

            if (Scheduler.getSafePlayer((((Player) event.getEntity()).getName())) && timers.isInFlyCheckArray(p)) {

                p.sendMessage(Messages.flyTempBan(p));
                PlayerInfo.setFlyTempBan(p);
                Flight.disableFlight(p, config.getTier(p), 8);

            }
        }

    }

    public void onPlayerDamageReceiveEvent(EntityDamageByEntityEvent event) {

        if (event.getDamager() instanceof Player) {

            Player p = ((Player) event.getDamager());

            if (Scheduler.getSafePlayer((((Player) event.getDamager()).getName())) && timers.isInFlyCheckArray(p)) {

                p.sendMessage(Messages.flyTempBan(p));
                PlayerInfo.setFlyTempBan(p);

                Flight.disableFlight(p, config.getTier(p), 8);

            }
        }

    }

    // Kick event
    public void onPlayerKick(PlayerKickEvent event) {
        Player p = event.getPlayer();
        int tier = Configuration.getTier(p);
        timers.removeFromArrays(p);
        timers.removeWithdrawArray(p, tier);
    }

    // Player fall damage event
    public void onPlayerFallDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {

            Player p = ((Player) event.getEntity());
            if (event.getCause() == DamageCause.FALL && Scheduler.getSafePlayer((((Player) event.getEntity()).getName()))
                    && (!p.hasPermission("flyp.takedamage"))) {

                event.setCancelled(true);

                Scheduler.removeSafePlayer(p.getName());

            }
        }

    }

    public void onPlayerPotionSplashEvent(PotionSplashEvent event) {
        // if player uses potion, cancles poton event and disables flight
        if (event.getEntity() instanceof Player) {

            Player p = ((Player) event.getEntity());

            if (Scheduler.getSafePlayer((((Player) event.getEntity()).getName())) && timers.isInFlyCheckArray(p)) {

                event.setCancelled(true);
                p.sendMessage(Messages.flyTempBan(p));
                PlayerInfo.setFlyTempBan(p);

                Flight.disableFlight(p, config.getTier(p), 8);

            }
        }

    }
    

    // player fall damage event
    @EventHandler(priority = EventPriority.HIGHEST)
    public void playerFallDamageEvent(EntityDamageEvent event) {

        onPlayerFallDamage(event);

    }

    // player quit event
    @EventHandler(priority = EventPriority.HIGHEST)
    public void playerQuitEvent(PlayerQuitEvent event) {

        onPlayerQuit(event);

    }

    // player kick event
    @EventHandler(priority = EventPriority.HIGHEST)
    public void playerKickEvent(PlayerKickEvent event) {

        onPlayerKick(event);

    }
}
